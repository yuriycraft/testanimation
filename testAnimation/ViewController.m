//
//  ViewController.m
//  testAnimation
//
//  Created by book on 25.03.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UICollisionBehaviorDelegate,UIDynamicAnimatorDelegate>

@property(strong, nonatomic) UIDynamicAnimator *animator;
@property(strong, nonatomic) UIGravityBehavior *gravity;
@property(strong, nonatomic) UICollisionBehavior *collision;

@end

@implementation ViewController {
    NSTimer *autoTimer;
    BOOL gameOver;
    BOOL pause;
    NSUInteger countBoxs;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
       gameOver= NO;
    pause = NO;
    
    
    [self initAnimation];
    
}


#pragma mark Colision delegate

- (void)collisionBehavior:(UICollisionBehavior *)behavior
      beganContactForItem:(id<UIDynamicItem>)item
   withBoundaryIdentifier:(id<NSCopying>)identifier
                  atPoint:(CGPoint)p {
    
    if ([(NSString *)identifier isEqualToString:@"outer-edge"]) {
        
        gameOver = YES;
        
        NSLog(@"Stop");
        
        [_gravity removeItem:item];
        [_collision removeItem:item];
        [_animator removeAllBehaviors];
        [autoTimer invalidate];
        
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.alertTitle = [NSString stringWithFormat:@"%lu box count!",(unsigned long)countBoxs];
        localNotification.alertBody = @"Game Over!";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = 1;
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        
        [[UIApplication sharedApplication]
         scheduleLocalNotification:localNotification];
        [_startButton setTitle:@"Start"];
    }
}

#pragma mark - Actions

- (IBAction)startButtonAction:(id)sender {
    if (gameOver) {
        gameOver=NO;
        countBoxs = 0;
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        
        
        [self initAnimation];
        self.collision.translatesReferenceBoundsIntoBoundary = YES;
        
        
        
        [self startTimer];
        [_startButton setTitle:@"Pause"];
        
    }else if(!gameOver && pause){
        [_startButton setTitle:@"Resume"];
        [autoTimer invalidate];
        pause = NO;
    }else if (!gameOver && !pause){
        [_startButton setTitle:@"Pause"];
        [self startTimer];
    }
    
    
}
-(void)initAnimation {
    self.animator.delegate=self;
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    self.collision = [[UICollisionBehavior alloc] init];
    self.gravity = [[UIGravityBehavior alloc] init];
    
    _collision.collisionDelegate = self;
    
    UIView *barrier = [[UIView alloc]
                       initWithFrame:CGRectMake(0, 15, self.view.bounds.size.width, 1)];
    barrier.backgroundColor = [UIColor redColor];
    [_collision
     addBoundaryWithIdentifier:@"outer-edge"
     fromPoint:CGPointMake(0, 0)
     toPoint:CGPointMake(barrier.bounds.size.width, 0)];
    [self.view addSubview:barrier];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    
}
-(void)startTimer {
    
    if (!gameOver) {
        pause =YES;
        
        autoTimer = [NSTimer scheduledTimerWithTimeInterval:0.5
                                                     target:self
                                                   selector:@selector(createAndShowAnimateViews)
                                                   userInfo:nil
                                                    repeats:YES];
    }
}

- (void)createAndShowAnimateViews {
    
    countBoxs = countBoxs+1;
    // Create sub views
    
    UIView *view;
    
    view = [[UIView alloc]
            initWithFrame:CGRectMake(arc4random() % 280, 16, 100, 100)];
    
    view.backgroundColor = [UIColor colorWithRed:(rand() % 255) / 255.0f
                                           green:(rand() % 255) / 255.0f
                                            blue:(rand() % 255) / 255.0f
                                           alpha:1.0f];
    //  view.transform =CGAffineTransformRotate(view.transform, (-M_PI_4/2));
    [self.view addSubview:view];
    
    // Add behaviors
    [self.collision addItem:view];
    [self.gravity addItem:view];
    [self.animator addBehavior:self.gravity];
    [self.animator addBehavior:self.collision];
    
}

@end
