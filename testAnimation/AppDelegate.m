//
//  AppDelegate.m
//  testAnimation
//
//  Created by book on 25.03.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()
@end

@implementation AppDelegate {

  UILocalNotification *localNotification;
}

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  // Override point for customization after application launch.

  if ([UIApplication
          instancesRespondToSelector:@selector(
                                         registerUserNotificationSettings:)]) {
    [application registerUserNotificationSettings:
                     [UIUserNotificationSettings
                         settingsForTypes:UIUserNotificationTypeAlert |
                                          UIUserNotificationTypeSound |
                      UIUserNotificationTypeBadge
                               categories:nil]];
  
  }
  application.applicationIconBadgeNumber = 0;
    
    
    
  return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
  // Sent when the application is about to move from active to inactive state.
  // This can occur for certain types of temporary interruptions (such as an
  // incoming phone call or SMS message) or when the user quits the application
  // and it begins the transition to the background state.
  // Use this method to pause ongoing tasks, disable timers, and throttle down
  // OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
  // Use this method to release shared resources, save user data, invalidate
  // timers, and store enough application state information to restore your
  // application to its current state in case it is terminated later.
  // If your application supports background execution, this method is called
  // instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
  // Called as part of the transition from the background to the inactive state;
  // here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
  // Restart any tasks that were paused (or not yet started) while the
  // application was inactive. If the application was previously in the
  // background, optionally refresh the user interface.
  application.applicationIconBadgeNumber = 0;
    [[UIApplication sharedApplication]cancelLocalNotification:localNotification];
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
  // Called when the application is about to terminate. Save data if
  // appropriate. See also applicationDidEnterBackground:.
    application.applicationIconBadgeNumber = 0;
    [[UIApplication sharedApplication]cancelLocalNotification:localNotification];
}

- (void)application:(UIApplication *)application
    didReceiveLocalNotification:(UILocalNotification *)notification {

  UIAlertController *alert =
      [UIAlertController alertControllerWithTitle:notification.alertTitle
                                          message:notification.alertBody
                                   preferredStyle:UIAlertControllerStyleAlert];

  UIAlertAction *defaultAction =
      [UIAlertAction actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction *action){
                             }];

  [alert addAction:defaultAction];

  UIWindow *alertWindow =
      [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  alertWindow.rootViewController = [[UIViewController alloc] init];
  alertWindow.windowLevel = UIWindowLevelAlert + 1;
  [alertWindow makeKeyAndVisible];
  [alertWindow.rootViewController presentViewController:alert
                                               animated:YES
                                             completion:^{

                                             }];
}

@end
