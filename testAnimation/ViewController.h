//
//  ViewController.h
//  testAnimation
//
//  Created by book on 25.03.16.
//  Copyright © 2016 YuriyCraft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *startButton;

- (IBAction)startButtonAction:(id)sender;

@end

